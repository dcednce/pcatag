# PCAtag Examples


[32snp.txt](32snp.txt)

Input data file with 32 snps

[32snpMstepG.short](32snpMstepG.short)

Short output with multi-step and genotype analysis from 32snp.txt input.

[32snpMstepG.long](32snpMstepG.long)

Long output with multi-step and genotype analysis from 32snp.txt input.

[32snp2stepG.short](32snp2stepG.short)

Short output with 2-step and genotype analysis from 32snp.txt input.

[32snp2stepG.long](32snp2stepG.long)

Long output with 2-step and genotype analysis from 32snp.txt input.

[32snpMstepH.short](32snpMstepH.short)

Short output with multi-step and haplotype analysis from 32snp.txt input.

[32snpMstepH.long](32snpMstepH.long)

Long output with multi-step and haplotype analysis from 32snp.txt input.

[32snp2stepH.short](32snp2stepH.short)

Short output with 2-step and haplotype analysis from 32snp.txt input.

[32snp2stepH.long](32snp2stepH.long)

Long output with 2-step and haplotype analysis from 32snp.txt input.

[32snpG.2summary](32snpG.2summary)

Output only the final tagging SNPs name with 2-step and genotype analysis from the 32snp.txt input.

[32snpH.Msummary](32snpH.Msummary)

Output only the final tagging SNPs name with multi-step and haplotype analysis from the 32snp.txt input.

[Home](index.html)    [Execute PCAtag](execute.html)    [Requirements](requirements.html)
