# Execute PCAtag

* Starting the GUI
    

* PC - Double click on the PCAtag.jar file
* Linux - at the PCAtag directory type _"java -jar PCAtag.jar"_

The display panel is separated into 2 sections. The top section is for entering optional variables. The bottom section is for outputing messages.

**Running Analysis**

1.  Enter an input data file name to the **_Input File Name_** text field or use the **_Browse_** button to select a file.
2.  Analysis Option

1.  **_Haplotype_** default option. This option first phases the data and then performs the PCA analyses on the phased data. It imputes missing data, so there are no issues of missing data.
2.  **_Genotype_** This option omits the phasing and performs the PCA analysis directly. This option should only be considered if the missing data rate is very low.
3.  **_Subset Analysis_** \- Use this option to execute analyses separately. This option requires the input data file to be setup with subset column.

4.  PCA Parameters

1.  **_EigenValue Threshold_** , default is 0.7. Indicates the eigenvalue threshold used to extract factors in the PCA analysis. Factor that have lower eigenvalues will only be selected if the percentage of the variance explained has not surpassed the threshold set for the variance explained, in which case more factors will be acquired to reach this threshold. Value should between 0.0 to 1.9.
2.  **_Variance Explained %_** , default value is 90%. And additional threshold used to extract additional facators in the PCA analysis (see above eigenvalue threshold for more info).

6.  Factor Loading

1.  **_Retain_** , default value is 0.4. Factor loading threshold for group membership. By default, a SNP is only considered to belong to a factor if its factor loading is >= 0.4 or <= 0.4. This value for a factor loading is a standard in the field (Stevens JP (1992) Applied Multiivariate Statistics for the Social Sciences, 2nd Edition, Hillsdale, NJ: Erlbaum).
2.  **_Surpress Print Below_**, default value is 0.2. Suppression for printing, factor loading are not printed in the output if they are <= 0.2 or >= 0.2.

8.  Analysis Method

1.  **_two step_**, 2-step PCA method.
2.  **_multi step_**, this is the default option. Run multi-step PCA method
3.  **_two step & multi step_**, run both 2-step and multi-step PCA methods.

10.  Click on the **_Uploading file_** button to verify the input data file format and subset option
11.  Output File

1.  Enter the output file name in the **_Output File Name_** text field or use the **_Browse_** button to select and overwrite an existing file or select a directory and enter the file name in the **_Selection_** text field in the Open File pop-up window.
2.  Print Option

1.  **_Long Output_** Long output format print enigenvalues and cumulatiive variance of the initial PCA analysis to determine the main factors. It also print all the details of the sub-factors and all the factor loadings, and the final factors and the tagging SNPs selected.
2.  **_short Output_** Short format output the eigenvalue and cumulative variance of the initial PCA analysis to determine the main factors. And the Final factor and the tagging SNPs selected.
3.  **_Summary_** A simply summary file output contains the final set of tagging SNPs. .

* Command Prompt:
    

**From the PCAtag directory type**
java -jar PCAtag.jar \-i inputfilename \-o outputfilename \[OPTION\]...

*   **Options Description:**

*   \-i <input file name> Name of the input file from which to read the genotype data.
*   \-o <output file name> Name of the file to which output is directed.
*   \-g for genotype, default is haplotype. Haplotype option first phases the data and then performs the PCA analyses on the phased data. The genotype option omits the phasing and performs the PCA analysis directly. the haplotype option imputes missing data, so there are no issues of missing data. The genotype option should only be considered if the missing data rate is very low.
*   \-s Use this option to execute analyses separately for each subset.Default is no subset analysis
*   \-e \[0.0..1.0\], default is 0.7. Eigenvalue threshold used to extract factors in the PCA analysis. Factor that have lower eigenvalues will only be selected if the percentage of the variance explained has not surpassed the threshold set for the variance explained (\-v), in which case more factors will be acquired to reach this threshold.
*   \-v \[0..100%\], default value is 90%. Variance explained, an additional threshold used to extract additional factors in the PCA analysis ( see above \-e option ).
*   \-f \[0.0-1.0\], default value is 0.4. Factor loading threshold for group membership. By default, a SNP is only considered to belong to a factor if its factor loading is >= 0.4 or <= 0.4. This value for a factor loading is a standard in the field (Stevens JP (1992) Applied Multiivariate Statistics for the Social Sciences, 2nd Edition, Hillsdale, NJ: Erlbaum).
*   \-t \[0.0-1.0\], default value is 0.2. Suppression for printing, factor loading are not printed in the output if they are <= 0.2 or >= 0.2.
*   \-p short | long Output print mode, default is "short". Results can be printed in long or short format. Short format output the eigenvalues and cumulative variance of the initial PCA analysis to determine the main factors. And the Final factors and the tagging SNPs selected. The long format output additionally contains all the details of the sub-factors and all the factor loadings. Only one or other of these canbe selected.
*   \-summary \- A simply summary file containing the names of the final set of the tagging SNPs.
*   \-2 for two step PCA analysis, default is multi-step. Select the this option to run 2 step PCA method.
*   \-m for multi step PCA analysis. Select this option to run multi step PCA method. This is the default.
*   \-b for both two step & multi step PCA analysis. Select this option to run both 2-step and multi step PCA methods.

  

**Example of command line syntax:**

java -jar PCAtag.jar -i <input path> -o <output path> -b -g

[Home](pcaIndex) [Requirements](requirements) [Examples](examples)
