# PCAtag FAQ

**PCATag FAQ**

*   Is PCAtag compatible with windows 7 64 bit?  
    The software for PCAtag and R are compatible with windows 7 64 bit; however, the fastPHASE is not. To execute PCAtag the three packages are required, so currently, PCAtag cannot be executed on a Windows 7 64 bit machine.
*   Does it only run on JAVA 1.8 or is it compatible with the older versions?  
    It is only runs on JAVA 1.8 or higher.
*   Can PC users get the entire package directly from the website?  
    Yes, PC users can get the current version of fastPHASE, R, and the PCAtag software by directly downloading the PCAtag package from the download option in the "Getting Started" section.
*   Why do Linux user have to download R separately?  
    Due to the many different computer architecture there are many different copies of R for Linux. User must install the correct version of R for their server.
*   In the GUI output file section, what is the _Browse_ button?  
    A pop-up window will open when user click on this button. This allows user to either select an existing file, and the new output will overwrite it, or select a directory and enter the new file name in the _selection_ text field to create this new file for the output.

[Home](index.html)    [Execute PCAtag](execute.html)    [Requirements](requirements.html)
