.. PCAtag Documentation documentation master file, created by
   sphinx-quickstart on Fri Sep 11 08:52:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PCAtag Documentation
================================================

.. toctree::

  pcaIndex
  readme
  requirements
  faq
  examples
  download
  installationPage
  execute

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
