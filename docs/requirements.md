# PCAtag Requirements

  

**PCAtag Requirements**

[Java 1.8](http://java.com/en/download/index.jsp)

Java 1.8 or later version installed on your Java virtual machine

PCAtag software bundle

[Download](download.html) the PCAtag software bundle, this includes PCAtag.jar, fastPHASE and R which require to run PCAtag software.

Input data file

This input data file contains one header line with id, marker names(s) and optional subset column; and the remaining lines are for the genotype data - unique individual id, marker data and optinal subset name. Marker data should be entered as two separate columns of numbers separated by a space or tab. See [32snp.txt](32snp.txt) for detail.

**Additional Requirement**

 

[R](http://www.r-project.org/)

Linux users must download and install R separately.

[Home](index.html)    [Execute PCAtag](execute.html)    [Examples](examples.html)
