package edu.utah.camplab.pcatag.app;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

import edu.utah.camplab.pcatag.constants.UIConstants;
import edu.utah.camplab.pcatag.utils.StringUtils;


public class DataAnalyzer {

  private boolean	doSubsetAnalysis	  = false;
  private boolean	needsDecoding		  = false;
  private LinkedHashMap<String, Locus[]>[] data	  = null;
  private String	errorMessage		  = "";
  private String	dataType		  = "multiallelic";
  private String	locusNames		  = null;
  private String	subset			  = "overall,control,case";
  private String[] major			  = null;
  private String[] minor			  = null;
  private double[] majorFreq			  = null;
  private double[] minorFreq			  = null;
  private int[] validAlleles			  = null;
  private int[] missingAlleles			  = null;

  private		int[][] missingMarkerArr  = new int[3][];
  private		int[][] validMarkerArr    = new int[3][];
  private		String[][] majorAlleleArr = new String[3][];
  private		String[][] minorAlleleArr = new String[3][];
  private		double[][] majorFreqArr   = new double[3][];
  private		double[][] minorFreqArr   = new double[3][];
  private int		numOfMarker               = 0;
  private String[] markerNames			  = null;

  public DataAnalyzer(LinkedHashMap<String, Locus[]>[] dataIn, boolean doSubset) {
    data = dataIn;
    doSubsetAnalysis = doSubset;
    dataType = getDataType(data[0]);
    //    numOfMarker = data[0].size();

    // System.out.println("Data type is " + dataType + ".");
  }

  public boolean isValid() {
    try {
      if (!isInputValid(data[0],UIConstants.pca_OVERALL)) {
        return false;
      }
      else if (!isInputValid(data[1],UIConstants.pca_CONTROL)) {
        return false;
      }
      else if (!isInputValid(data[2], UIConstants.pca_CASE)) {
        return false;
      }
      else {
        return true;
      }
    } catch (Exception e) {
      errorMessage = "Input failed validation with a critical error.";
      e.printStackTrace();
      return false;
    }
  }

  public int[][] getmissingMarkerArr() {
    return this.missingMarkerArr;
  }

  public int[][] getvalidMarkerArr() {
    return this.validMarkerArr;
  }

  public String[][] getmajorAlleleArr() {
    return this.majorAlleleArr;
  }

  public String[][] getminorAlleleArr() {
    return this.minorAlleleArr;
  }

  public double[][] getmajorFreqArr() {
    return this.majorFreqArr;
  }

  public double[][] getminorFreqArr() {
    return this.minorFreqArr;
  }

  public int getNumOfMarkers() {
    if (numOfMarker == 0) {
      LinkedHashMap<String, Locus[]> lhm = getDataHashMap()[0];
      numOfMarker = lhm.get(lhm.keySet().iterator().next()).length;
    }
    return numOfMarker;
  }

  public String[] getMarkerNames() {
    return this.markerNames;
  }

  private boolean isInputValid( LinkedHashMap<String, Locus[]> data, String subset) {

    if (data == null || data.isEmpty()) {
      return true;
    }

    if (!areMarkersValid(data)) {
      return false;
    }

    if (dataType.equals("subsetmissing")) {
      errorMessage = "Cannot continue. Please check that there is no multiallelic loci data and, if subset analysis is checked, that there are 3 subsets: 0, 1, and 2.";
      return false;
    }
    else if (dataType.equals("multiallelic")) {
      errorMessage = "Variants with more than two alleles have been found. PCAtag allows only biallelic loci data.";
      return false;
    }
    else if (dataType.equals("basecoding")) {
      needsDecoding = true;
    }

    return true;
  }

  private void decodeMarkers(LinkedHashMap<String, Locus[]> subdata, String subset) {
    int subsetIndex = decodeSubset(subset);
    if (subdata != null && !subdata.isEmpty()) {
      int numMarkers = subdata.get(subdata.keySet().iterator().next()).length;

      if (subdata.get(subdata.keySet().iterator().next())[numMarkers - 1] == null) {
        numMarkers = numMarkers - 1;
      }
      numOfMarker = numMarkers;
      validAlleles = new int[numMarkers];
      missingAlleles = new int[numMarkers];
      major = new String[numMarkers];
      minor = new String[numMarkers];
      majorFreq = new double[numMarkers];
      minorFreq = new double[numMarkers];
      markerNames = new String[numMarkers];

      for (int imarker = 0; imarker < numMarkers; imarker++) {
	decodeMarker(subdata, imarker);
      }
    }
    
    missingMarkerArr[subsetIndex] = missingAlleles;
    validMarkerArr[subsetIndex] = validAlleles;
    majorAlleleArr[subsetIndex] = major;
    majorFreqArr[subsetIndex] = majorFreq;
    minorAlleleArr[subsetIndex] = minor;
    minorFreqArr[subsetIndex] = minorFreq;
  }

  private String getDataType(
			     LinkedHashMap<String, Locus[]> data) {
    Iterator<String> iter = data.keySet().iterator();
    if (iter.hasNext()) {
      String key = iter.next();
      for (int i = 0; i < data.get(key).length; i++) {
        Locus m = data.get(key)[i];
        if (m.GetAllele1().equals("0") && m.GetAllele2().equals("0")) {
          continue;
        }
        else {
          // Detect the file type
          if (m.GetAllele1().equals("G")
	      || m.GetAllele1().equals("C")
	      || m.GetAllele1().equals("T")
	      || m.GetAllele1().equals("A")) {
            needsDecoding = true;
            return "basecoding";
          }
          else if (m.GetAllele1().equals("0")
		   || m.GetAllele1().equals("1")
		   || m.GetAllele1().equals("2")) {
            return "numbers";
          }
          else if (m.GetAllele1().length() > 1) {
            return "multiallelic";
          }
        }
      }
    }

    // If none of the conditions were met, make this assumption
    return "subsetmissing";
  }

  private boolean areMarkersValid(
				  LinkedHashMap<String, Locus[]> data) {
    Iterator<String> iter = data.keySet().iterator();

    while (iter.hasNext()) {
      String key = iter.next();
      for (int i = 0; i < data.get(key).length; i++) {
        if (data.get(key)[i] != null) {
          Locus m = data.get(key)[i];
          if ((m.GetAllele1().equals("0") && !m.GetAllele2()
	       .equals("0"))
	      || (m.GetAllele2().equals("0") && !m.GetAllele1()
		  .equals("0"))) {
            errorMessage = "Alleles for marker " + m.GetName()
              + " and ID " + key.toString()
              + " must either be both present or both missing.";
          }
        }
      }
    }

    // All markers are valid
    return true;
  }

  public void printError() {
    System.out.println(errorMessage);
  }

  public void loadSilos() {
    decodeMarkers(data[0],UIConstants.pca_OVERALL);
    if (!doSubsetAnalysis) {
      System.out.println("Data successfully decoded.");
      // printHashedMap(data[0]);
    }
    else {
      System.out.println("Overall: Data successfully decoded.");
      // printHashedMap(data[0]);
      decodeMarkers(data[1],UIConstants.pca_CONTROL);
      System.out.println("Control: Data successfully decoded.");
      // printHashedMap(data[1]);
      decodeMarkers(data[2],UIConstants.pca_CASE);
      System.out.println("Case: Data successfully decoded.");
      // printHashedMap(data[2]);
    }
  }

  public boolean needsDecoding() {
    return needsDecoding;
  }

  private void decodeMarker(LinkedHashMap<String, Locus[]> subsetData, int mkrx) {

    DecimalFormat prec4 = new DecimalFormat("0.0000");
    TreeMap<String, DataAnalyzer.AlleleCounter> baseCodes = new TreeMap<>();
    initBaseCodes(baseCodes);

    int indivCount = 0;
    Iterator<String> iter = subsetData.keySet().iterator();
    while (iter.hasNext()) {
      Locus m = subsetData.get(iter.next())[mkrx];
      indivCount++;
      if (m == null) {
        continue;
      }

      markerNames[mkrx] = m.GetName();
      if (m.GetAllele1().equals("0") || m.GetAllele2().equals("0")) {
        missingAlleles[mkrx] += 1;
	if (m.GetAllele1().equals("0") && m.GetAllele2().equals("0")) {
	  missingAlleles[mkrx] += 1;
	}
      }
      else {
        validAlleles[mkrx] += 1;
      }
      if (! m.GetAllele1().equals("0")) {
	incrementAlleleCount(baseCodes, m.GetAllele1());
      }
      if (! m.GetAllele2().equals("0")) {
	incrementAlleleCount(baseCodes, m.GetAllele2());
      }
    }

    // Sort original TreeMap
    int mincount = 2 * indivCount;
    int maxcount = 0;
    AlleleCounter rareAllele = null;
    AlleleCounter commonAllele = null;
    for (Map.Entry<String, AlleleCounter> me : baseCodes.entrySet()) {
      AlleleCounter counter = me.getValue();
      if (counter.count < mincount) {
	mincount = counter.count;
	rareAllele = counter;
      }
      if (counter.count > maxcount) {
	maxcount = counter.count;
	commonAllele = counter;
      }
    }

    major[mkrx] = commonAllele.name;
    minor[mkrx] = rareAllele.name;

    String temp = prec4.format((double) (commonAllele.count  / (double)(2 * validAlleles[mkrx])));
    String temp2 = prec4.format((double) (rareAllele.count / (double) (2 * validAlleles[mkrx])));
    majorFreq[mkrx] = Double.parseDouble(temp);
    minorFreq[mkrx] = Double.parseDouble(temp2);

    /*
     * if (subsetAnalysis) System.out.println(subset + ": " + common + " is
     * the most common base code for marker: " + (i + 1) + "."); else
     * System.out.println(common + " is the most common base code for
     * marker: " + (i + 1) + ".");
     */

    // Replace base codes with numerical value
    iter = subsetData.keySet().iterator();
    if (needsDecoding) {
      while (iter.hasNext()) {
	Locus m = subsetData.get(iter.next())[mkrx];
	if (m == null) {
	  continue;
	}

	if (m.GetAllele1().equals(commonAllele.name)) {
	  m.SetAllele1("1");
	}
	else if (!m.GetAllele1().equals("0")) {
	  m.SetAllele1("2");
	}
	else {
	  m.SetAllele1("0");
	}

	if (m.GetAllele2().equals(commonAllele.name)) {
	  m.SetAllele2("1");
	}
	else if (!m.GetAllele2().equals("0")) {
	  m.SetAllele2("2");
	}
	else {
	  m.SetAllele2("0");
	}
      }
    }
  }

  public ArrayList<String> generatePhaseFile() {
    ArrayList<String> returnArrayList = new ArrayList<String>();
    String phaseFileNamePrefix = "phase" + System.currentTimeMillis();
    for (int i = 0; i < data.length; i++) {
      try {
        LinkedHashMap<String, Locus[]> linkedHashMap = data[i];
        Iterator<String> iter = linkedHashMap.keySet().iterator();

        String outputString = "";
        boolean firstIteration = true;
        while (iter.hasNext()) {
          String key = iter.next();

          Locus[] value = linkedHashMap.get(key);

          // get the lines of allele's
          String locusTypes = "";
          String line1 = "";
          String line2 = "";

          int totalCount = value.length;
          String space = "";
          for (int j = 0; j < totalCount; j++) {
            if (value[j] == null) {
              totalCount--;
              continue;
            }

            Locus m = value[j];

            line1 += space + m.GetAllele1();
            line2 += space + m.GetAllele2();
            locusTypes += m.GetName().charAt(0);

            space = " ";
          }
          line1 += StringUtils.LINE_SEPARATOR;
          line2 += StringUtils.LINE_SEPARATOR;
          locusTypes += StringUtils.LINE_SEPARATOR;

          if (firstIteration) {
            firstIteration = false;
            outputString += "" + linkedHashMap.keySet().size() + StringUtils.LINE_SEPARATOR;
            outputString += "" + totalCount + StringUtils.LINE_SEPARATOR;
            outputString += "" + locusTypes;
          }
          outputString += "#" + key + StringUtils.LINE_SEPARATOR;
          outputString += "" + line1;
          outputString += "" + line2;
        }

        if (!firstIteration) {
          String fileName = phaseFileNamePrefix + "_" + (i + 1) + "_in";
          // create the file writer
          BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
          bw.write(outputString);
          bw.close();

          returnArrayList.add(fileName);
        }
      } catch (IOException ioe) {
        throw new RuntimeException(ioe);
      }
    }
    return returnArrayList;
  }

  public static void printHashedMap(
				    LinkedHashMap<String, Locus[]> hmap) {
    if (hmap != null && !hmap.isEmpty()) {
      System.out.println("****************");

      Iterator<String> iter = hmap.keySet().iterator();
      while (iter.hasNext()) {
        String key = iter.next();

        Locus[] value = hmap.get(key);
        System.out.print(key + " ");
        for (int i = 0; i < value.length; i++) {
          if (value[i] != null) {
            Locus m = value[i];
            m.printLocus();
          }
        }
        System.out.println();
      }
      System.out.println("****************");
    }
  }

  public static void printDoubleMap(
				    LinkedHashMap<String, Double[]> hmap) {
    if (hmap != null && !hmap.isEmpty()) {
      System.out.println("****************");

      Iterator<String> iter = hmap.keySet().iterator();
      while (iter.hasNext()) {
        String key = iter.next();

        Double[] value = hmap.get(key);
        System.out.print(key + " ");
        for (int i = 0; i < value.length; i++) {
          System.out.print(value[i]);
          System.out.print(" ");
        }
        System.out.println();
      }
      System.out.println("****************");
    }
  }

  public ArrayList<String> createGenotypeFreqFile(LinkedHashMap<String, Locus[]> hmap) {
    String freqFileName = "freq.txt";
    int numMarkers = 0;
    ArrayList<String> result = new ArrayList<String>();

    if (hmap != null && !hmap.isEmpty()) {
      FileOutputStream fout;

      try {
        // Open an output stream
        fout = new FileOutputStream(freqFileName);
        PrintStream printStream = new PrintStream(fout);

        Iterator<String> iter = hmap.keySet().iterator();
        String key = iter.next();
        Locus[] value = hmap.get(key);
        for (int i = 0; i < value.length; i++) {
          if (value[i] != null) {
            numMarkers++;

            if (i == 0) {
              printStream.print(value[i].GetName());
            }
            else {
              printStream.print(" ");
              printStream.print(value[i].GetName());
            }
          }
        }

        iter = hmap.keySet().iterator();
        while (iter.hasNext()) {
          printStream.println("");
          key = iter.next();
          value = hmap.get(key);
          for (int i = 0; i < value.length; i++) {
            if (value[i] != null) {
              if (i == 0) {
                printStream.print(value[i].genotypeDecode());
              }
              else {
                printStream.print(" ");
                printStream.print(value[i].genotypeDecode());
              }
            }
            else {
	      System.err.println("Missing genotype  for " + key + " (zero'd) on line " + i);
              printStream.print(" 0");
            }
          }
          // Close our output stream
        }
        printStream.close();
        // Catches any error conditions
      } catch (IOException e) {
        System.err.println("Unable to write genotype frequency file.");
        System.exit(-1);
      }
      result.add(freqFileName);
      result.add(numMarkers + "");
    }
    return result;
  }

  public static void printArrayList(
				    ArrayList<String> list) {
    Iterator<String> iter = list.iterator();

    while (iter.hasNext())
      System.out.println(iter.next());
  }

  public static String getValueFromIndex( LinkedHashMap<String, Double[]> map, int index) {
    String key = "";
    Iterator<String> iter = map.keySet().iterator();

    for (int j = 0; j <= index; j++) {
      if (iter.hasNext()) {
	key = iter.next();
      }
    }

    return key;
  }

  public static Double getFactor( LinkedHashMap<String, Double[]> map, int row, int column) {
    Double result = 0.0;
    String key = "";
    Iterator<String> iter = map.keySet().iterator();
    Double values[] = null;

    try {
      for (int j = 0; j <= row; j++) {
	if (iter.hasNext()) {
	  key = iter.next();
	}
	values = map.get(key);

	for (int i = 0; i <= column; i++)
	  result = values[i];
      }
    } catch (Exception e) {
      result = null;
    }
    return result;
  }

  public LinkedHashMap<String, Locus[]>[] getDataHashMap() {
    return data;
  }

  public ArrayList<String> getMakerNamesForIndex( int i) {
    LinkedHashMap<String, Locus[]> linkedHashMap = data[i];
    Iterator<String> iter = linkedHashMap.keySet().iterator();
    
    ArrayList<String> markerNames = new ArrayList<String>();
    if (iter.hasNext()) {
      String key = iter.next();
      Locus[] value = linkedHashMap.get(key);
      for (int j = 0; j < value.length; j++) {
	if (value[j] == null) {
	  continue;
	}

	markerNames.add(value[j].GetName());
      }
    }

    return markerNames;
  }

  private int decodeSubset(String subname) {
    if (subname.equalsIgnoreCase(UIConstants.pca_OVERALL)) {
      return 0;
    }
    if (subname.equalsIgnoreCase(UIConstants.pca_CONTROL)) {
      return 1;
    }
    if (subname.equalsIgnoreCase(UIConstants.pca_CASE)) {
      return 2;
    }
    return -1;  //that'll confuse 'em;
  }
      
  private void initBaseCodes( TreeMap<String, AlleleCounter> codings) {
    if (needsDecoding) {
      codings.put("A",new AlleleCounter("A"));
      codings.put("C",new AlleleCounter("C"));
      codings.put("G",new AlleleCounter("G"));
      codings.put("T",new AlleleCounter("T"));
    }
    else {
      codings.put("1",new AlleleCounter("1"));
      codings.put("2",new AlleleCounter("2"));
    }
  }

  private void incrementAlleleCount(TreeMap<String, AlleleCounter> codings, String allele) {
    AlleleCounter counter = codings.get(allele);
    counter.count++;
  }

  private class AlleleCounter {
    public String name;
    public int count = 0;
    public double freq = 0.0;
    public AlleleCounter(String nm) {
      name = nm;
    }
  }
}
