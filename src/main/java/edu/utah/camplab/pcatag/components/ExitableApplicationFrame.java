package edu.utah.camplab.pcatag.components;

import javax.swing.JFrame;
import java.awt.*;


public class ExitableApplicationFrame extends JFrame {

  private ExitableApplicationFrame() {

  }

  public ExitableApplicationFrame(
    String title) {
    super(title);
    super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    super.setSize(800, 800);
    super.setVisible(true);
  }

  public Component getComponent() {
    int i = 0;
    while (i < this.getComponentCount()) {
      Component c = getComponent(i);
      if (c instanceof Container) {
        return c;
      }
    }
    return this;
  }
}
