package edu.utah.camplab.pcatag.constants;

public class UIConstants
{
  public static String s_ADVANCED_OPTIONS = "Advanced Options";
  public static String s_ANALYSIS_OPTIONS = "Analysis Options";
  public static String s_BROWSE = "Browse";
  public static String s_CONTINUE_TO_ANALYSIS = "Continue To Analysis";
  public static String s_CORRELATION_COVARIANCE_MATRICES = "Correlation/Covariance Matrices";
  public static String s_CUMULATIVE_EIGENVALUE_GRAPH = "Cumulative Eigenvalue Graph";
  public static String s_EIGENVALUE_THRESHOLD = "Eigenvalue Threshold:";
  public static String s_FACTOR_LOADING = "Factor Loading";
  public static String s_GENOTYPE = "Genotype";
  public static String s_HAPLOTYPE = "Haplotype";
  public static String s_INPUT_FILE = "Input File";
  public static String s_INPUT_FILE_NAME = "Input File Name";
  public static String s_INPUT_OUTPUT_FILE_NAMES = "Input/Output File Names";
  public static String s_LONG_OUTPUT = "Long Output";
  public static String s_OUTPUT_FILE = "Output File";
  public static String s_OUTPUT_FILE_NAME = "Output File Name";
  public static String s_OPTIONS = "Options";
  public static String s_PCA_PARAMETERS = "PCA Parameters";
  public static String s_PCA_TAG = "PCAtag";
  public static String s_RETAIN = "Retain";
  public static String s_ROTATION = "Rotation";
  public static String s_RUN = "Run";
  public static String s_SCREE_PLOT = "Scree-Plot";
  public static String s_SUBSET_ANALYSIS = "Subset Analysis";
  public static String s_SUPRESS_PRINT_BELOW = "Supress Print Below";
  public static String s_UPLOAD_FILE = "Upload File";
  public static String s_VARIANCE_EXPLAINED = "% Variance Accounted:";
  public static String s_VARIMAX = "Varimax";

  // In the spirit of filenames being a form of user interface
  // FP for fastPHASE
  public static String pca_FP_OUTPREF = "phase";
  public static String pca_CASE = "case";
  public static String pca_CONTROL = "control";
  public static String pca_OVERALL = "overall";
}
