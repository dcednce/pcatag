package edu.utah.camplab.pcatag.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import edu.utah.camplab.pcatag.app.PCATagView;
import edu.utah.camplab.pcatag.components.VerticalLayout;
import edu.utah.camplab.pcatag.constants.UIConstants;


public class InputOutputFilesPanel
  extends JPanel
  implements ActionListener {
  private JButton m_inputFileBrowseButton = new JButton(UIConstants.s_BROWSE);
  private JButton m_uploadFileButton = new JButton(UIConstants.s_UPLOAD_FILE);
  private JButton m_outputFileBrowseButton = new JButton(UIConstants.s_BROWSE);
  private JButton m_continueToAnalysisButton = new JButton(UIConstants.s_CONTINUE_TO_ANALYSIS);
  private JTextField m_inputFileNameField = new JTextField(15);
  private JTextField m_outputFileNameField = new JTextField(15);
  private JCheckBox m_longOutputCheckBox = null;
  private PCATagView m_view = null;

  private static final int PANEL_WIDTH = 550;
  private static final int PANEL_HEIGHT = 120;

  /**
   *
   */
  public InputOutputFilesPanel(
    PCATagView frame,
    String inputFileName,
    String outputFileName,
    boolean longPrintMode) {
    m_view = frame;
    createUI(longPrintMode);
    m_inputFileNameField.setText(inputFileName);
    m_outputFileNameField.setText(outputFileName);
  }

  private void createUI(
    boolean longPrintMode) {
    JPanel inputPanel = new JPanel();
    {
      JPanel topPanel = new JPanel();
      {
        topPanel.add(new JLabel(UIConstants.s_INPUT_FILE_NAME));
        topPanel.add(m_inputFileNameField);
        topPanel.add(m_inputFileBrowseButton);
        topPanel.add(m_uploadFileButton);
        m_inputFileBrowseButton.addActionListener(this);
        m_uploadFileButton.addActionListener(this);
      }
      JPanel bottomPanel = new JPanel();
      {
        m_continueToAnalysisButton.addActionListener(this);
        bottomPanel.add(new JLabel("                                                            "));
        bottomPanel.add(m_continueToAnalysisButton);
      }

      inputPanel.setLayout(new VerticalLayout());
      inputPanel.add(topPanel);
      inputPanel.add(bottomPanel);
      inputPanel.setBorder(BorderFactory
        .createTitledBorder(UIConstants.s_INPUT_FILE));
      inputPanel
        .setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    }

    JPanel outputPanel = new JPanel();
    {
      JPanel topPanel = new JPanel();
      {
        topPanel.add(new JLabel(UIConstants.s_OUTPUT_FILE_NAME));
        topPanel.add(m_outputFileNameField);
        topPanel.add(m_outputFileBrowseButton);
        m_outputFileBrowseButton.addActionListener(this);
      }
      JPanel bottomPanel = new JPanel();
      {
        m_longOutputCheckBox = new JCheckBox(UIConstants.s_LONG_OUTPUT,
          longPrintMode);
        bottomPanel.add(m_longOutputCheckBox);
        bottomPanel
          .add(new JLabel("                                                                   "));
      }
      outputPanel.setBorder(BorderFactory
        .createTitledBorder(UIConstants.s_OUTPUT_FILE));
      outputPanel.setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
      outputPanel.setLayout(new VerticalLayout());
      outputPanel.add(topPanel);
      outputPanel.add(bottomPanel);
    }
    goToinitState();
    add(inputPanel);
    add(outputPanel);
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == m_inputFileBrowseButton) {
      JFileChooser fileChooser = new JFileChooser(System
        .getProperty("user.dir"));
      int returnVal = fileChooser.showOpenDialog(m_view.getFrame());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File selFile = fileChooser.getSelectedFile();
        m_inputFileNameField.setText(selFile.getPath());
        goToReadyForUploadState();
      }
      else {
        goToinitState();
      }
    }
    else if (e.getSource() == m_outputFileBrowseButton) {
      JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
      int returnVal = fileChooser.showOpenDialog(m_view.getFrame());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File selFile = fileChooser.getSelectedFile();
        m_outputFileNameField.setText(selFile.getPath());
        goToReadyContinueToAnalysis();
      }
    }
    else if (e.getSource() == m_continueToAnalysisButton) {
      if (m_inputFileNameField.getText().equals(m_outputFileNameField.getText())) {
        System.out.println("Input and output files cannot be the same.");
      }
      else if (!new File(m_inputFileNameField.getText()).exists()) {
        System.out.println("Input file path is not valid.");
      }

      else if (!new File(new File(m_outputFileNameField.getText()).getParent()).isDirectory()) {
        System.out.println("Output file path is not valid.");
      }

      m_view.runPCATag(m_inputFileNameField.getText(), m_outputFileNameField.getText());
    }
    else if (e.getSource() == m_uploadFileButton) {
      m_view.clearOutput();

      if (m_view.validateInput() == null) {
        System.out.println("File is not valid. Could not upload file.");
        goToReadyForUploadState();
      }
      else {
        System.out.println("File uploaded successfully.");
        goToReadyForOutputBrowse();
      }
    }
  }

  public String getInputFileName() {
    return m_inputFileNameField.getText();
  }

  public String getOutputFileName() {
    return m_outputFileNameField.getText();
  }

  public boolean isLongOutput() {
    return m_longOutputCheckBox.isSelected();
  }

  private void goToinitState() {
    m_outputFileNameField.setText("");
    m_inputFileBrowseButton.setEnabled(true);
    m_uploadFileButton.setEnabled(false);
    m_outputFileBrowseButton.setEnabled(false);
    m_continueToAnalysisButton.setEnabled(false);
  }

  private void goToReadyForUploadState() {
    m_outputFileNameField.setText("");
    m_inputFileBrowseButton.setEnabled(true);
    m_uploadFileButton.setEnabled(true);
    m_outputFileBrowseButton.setEnabled(false);
    m_continueToAnalysisButton.setEnabled(false);
  }

  private void goToReadyForOutputBrowse() {
    m_outputFileNameField.setText("");
    m_inputFileBrowseButton.setEnabled(true);
    m_uploadFileButton.setEnabled(true);
    m_outputFileBrowseButton.setEnabled(true);
    m_continueToAnalysisButton.setEnabled(false);
  }

  private void goToReadyContinueToAnalysis() {
    m_inputFileBrowseButton.setEnabled(true);
    m_uploadFileButton.setEnabled(true);
    m_outputFileBrowseButton.setEnabled(true);
    m_continueToAnalysisButton.setEnabled(true);
  }
}
